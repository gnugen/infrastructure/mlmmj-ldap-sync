PREFIX ?= /usr

build:

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin $(DESTDIR)/etc $(DESTDIR)/lib/systemd/system
	install -m755 mlmmj-ldap-sync $(DESTDIR)$(PREFIX)/bin/mlmmj-ldap-sync
	install -m644 conf.example.toml $(DESTDIR)/etc/mlmmj-ldap-sync.toml
	install -m644 mlmmj-ldap-sync.service mlmmj-ldap-sync.timer $(DESTDIR)/lib/systemd/system
	install -m644 mlmmj-ldap-sync@.service mlmmj-ldap-sync@.timer $(DESTDIR)/lib/systemd/system
